package app;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class App {
    public static void main(String[] args) {
        ReentrantLock reeLock = new ReentrantLock();
        Condition produttore = reeLock.newCondition();
        Condition consumatore = reeLock.newCondition();

        while (true) {
            try {
                try {
                    for (int i = 0; i < 3; i++) {
                        System.out.println("wait " + i);
                        consumatore.await(500, TimeUnit.MILLISECONDS);
                    }
                    produttore.signal();
                } finally {
                    reeLock.unlock();
                }
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}